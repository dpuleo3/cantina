class Client
  attr_accessor :name, :lastname, :id, :code, :history
  attr_reader :balance
  def initialize name, lastname, id, code, balance = 0
    @name = name
    @lastname = lastname
    @id = id
    @code = code
    @balance = balance
    @history = []
  end

  def pay amount, code 
    if (code = @code)
      @balance -= amount
      return true
    end
    false
  end

  def add_to_balance amount 
    @balance += amount
  end

  def register_sale sale
    @history << sale
  end 

end