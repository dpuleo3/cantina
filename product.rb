class Product

attr_accessor :name, :category, :cost, :profit

  def initialize name, category, cost, profit, quantity
    @name = name 
    @category = category
    @cost = cost
    @profit = 0.3
    @quantity = quantity
  end

  def price 
    @cost / (1 - (@profit / 100)) 
  end

  def add_inventory quantity
    @quantity += quantity
    @quantity
  end

  def discount_inventory quantity
    if (@quantity >= quantity)
      @quantity -= quantity
      return true
    end
    false
  end

  def available? quantity
    @quantity >= quantity
  end

end